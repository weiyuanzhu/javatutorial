package jfacetutorial.TableViewers;


import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import jfacetutorial.model.Article;
import jfacetutorial.provider.AbstractTableContentLabelProvider;

public class ArticleTableCLProvider extends AbstractTableContentLabelProvider {
	
	private Image image;

	public ArticleTableCLProvider() {
		LocalResourceManager jfaceRsManager = new LocalResourceManager(JFaceResources.getResources(),
												Display.getCurrent().getShells()[0]); 
	
		ImageDescriptor imageDescriptor = ImageDescriptor.createFromFile(ArticleTableCLProvider.class, "/jfacetutorial/image/check.png");
	
		image = jfaceRsManager.createImage(imageDescriptor);
		
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {		
		Article article = (Article) element;
		
		switch (columnIndex) {
		
		case 0:
			return article.getTitle();
		case 1:
			return article.getAuthor();
	    default:
	    	return null;
		}			
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		Article article = (Article) element;
		
		switch (columnIndex) {
		case 0:
			if (article.isPublished()) {
				return getScaledImage(0.1);
			}
		default: 
			return null;
		}	
	}

	@Override
	public Object[] getElements(Object inputElement) {
		@SuppressWarnings("unchecked")
		List<Article> list = (List<Article>) inputElement;
		return list.toArray();
	}
	
	private final Image getScaledImage(double ratio) {
		final int width = image.getBounds().width;
		final int height = image.getBounds().height;
		return new Image(Display.getCurrent(),image.getImageData().scaledTo((int)(width * ratio), (int)(height * ratio)));
	}
	

}
