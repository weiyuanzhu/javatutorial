package jfacetutorial.decoration;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;

public class DecorationDemo {

	protected Shell shell;
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DecorationDemo window = new DecorationDemo();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		shell.setLayout(new GridLayout(5, false));
		
		Image image = FieldDecorationRegistry.
                getDefault().
                getFieldDecoration(FieldDecorationRegistry.DEC_INFORMATION).
                getImage();
     
      new Label(shell, SWT.NONE);
      new Label(shell, SWT.NONE);
      new Label(shell, SWT.NONE);
      
      text = new Text(shell, SWT.BORDER);
      
      ControlDecoration controlDecoration = new ControlDecoration(text, SWT.LEFT | SWT.TOP);
      controlDecoration.setDescriptionText("Some description");
      
      controlDecoration.setImage(image);

	}

}
