package jfacetutorial.tree;

import java.util.List;

import jfacetutorial.model.AppMenu;
import jfacetutorial.model.DataModel;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.jface.viewers.TreeViewer;

public class TreeViewerDemo {

	protected Shell shell;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TreeViewerDemo window = new TreeViewerDemo();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	@SuppressWarnings("unchecked")
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TreeViewer treeViewer = new TreeViewer(shell, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		tree.setHeaderVisible(true);
		
		
		//Content and label provider
		AppMenuTreeCLProrider provider = new AppMenuTreeCLProrider();
		treeViewer.setContentProvider(provider);
		treeViewer.setLabelProvider(provider);
		
		//data source
		List<AppMenu> appMenuList = DataModel.getAppMenus();
		treeViewer.setInput(appMenuList);
		
		List<AppMenu> input = (List<AppMenu>) treeViewer.getInput();
		AppMenu appMenu = input.get(0);
		appMenu.addChildren(new AppMenu("Test", "Test menu",null));
		
		treeViewer.refresh();
	}

}
