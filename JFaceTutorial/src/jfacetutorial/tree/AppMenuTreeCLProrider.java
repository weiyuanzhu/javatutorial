package jfacetutorial.tree;

import java.util.List;

import jfacetutorial.model.AppMenu;
import jfacetutorial.provider.AbstractTreeContentLabelProvider;

public class AppMenuTreeCLProrider extends AbstractTreeContentLabelProvider {

	@Override
	public String getText(Object element) {
		AppMenu appMenu = (AppMenu) element;
		return appMenu.getMenuTitle();
	}

	@Override
	public Object[] getElements(Object inputElement) {
		@SuppressWarnings("unchecked")
		List<AppMenu> appMenuList = (List<AppMenu>) inputElement;
		return appMenuList.toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		AppMenu appMenu = (AppMenu) parentElement;
		//return appMenu.getChildren().toArray();
		return appMenu.getChildren() == null? null : appMenu.getChildren().toArray();
	}

	@Override
	public boolean hasChildren(Object element) {
		AppMenu appMenu = (AppMenu) element;
		List<AppMenu> children = appMenu.getChildren();
		
		return children != null && children.size() != 0;
	}
	
	

}
