package jfacetutorial.treetable;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import jfacetutorial.model.Department;
import jfacetutorial.model.Employee;
import jfacetutorial.provider.AbstractTreeTableContentLableProvider;

public class DeptEmpTreeTableCLProvider extends
		AbstractTreeTableContentLableProvider {
	
	private Image DEPT_IMG;
	private Image EMP_IMG;
	
	public DeptEmpTreeTableCLProvider() {
		DEPT_IMG = Display.getCurrent().getSystemImage(SWT.ICON_INFORMATION);
		EMP_IMG = Display.getCurrent().getSystemImage(SWT.ICON_ERROR);
	}

	@Override
	public Object[] getElements(Object inputElement) {
		@SuppressWarnings("unchecked")
		List<Department> departments = (List<Department>) inputElement;
		return departments.toArray();
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Department) {
			Department dept = (Department) element;
			List<Employee> empList = dept.getEmployees();
			
			//return true only if empList is not null and has 1 or more elements
			return  empList != null && empList.size() != 0;
			
		} else {
			//element is not a Department object
			return false;
		}
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof Department) {
			Department dept = (Department) parentElement;
			List<Employee> empList = dept.getEmployees();
			
			return empList == null? null : empList.toArray();
			
		} else {
			//element is not a Department object
			return null;
		}
	}
	
	@Override
	 public String getColumnText(Object element, int columnIndex) {
	     if (element instanceof Department) {
	         Department dept = (Department) element;
	         switch (columnIndex) {
	         case 0:
	             return dept.getDeptNo();
//	         case 1:
//	             return dept.getDeptName();
	         default:
	             return null;
	         }
	     } else {
	         Employee emp = (Employee) element;
	         switch (columnIndex) {
	         case 1:
	             return emp.getEmpNo();
	         case 2:
	             return emp.getFirstName();
	         case 3:
	             return emp.getLastName();
	         default:
	             return null;
	         }
	     }
	 }
	 
	 @Override
	 public Image getColumnImage(Object element, int columnIndex) {
	     if (element instanceof Department) {
	         switch (columnIndex) {
	         case 0:
	             return DEPT_IMG;
	         default:
	             return null;
	         }
	     } else {
	         switch (columnIndex) {
	         case 1:
	             return EMP_IMG;
	         default:
	             return null;
	         }
	     }
	 }
	
	
	
}
