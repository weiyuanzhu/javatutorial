package jfacetutorial.treetable;

import java.util.List;

import jfacetutorial.model.DataModel;
import jfacetutorial.model.Department;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.TreeColumn;

public class TreeTableViewerDemo {

	protected Shell shell;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TreeTableViewerDemo window = new TreeTableViewerDemo();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(570, 400);
		shell.setText("SWT Application");
		shell.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		TreeViewer treeViewer = new TreeViewer(shell, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		tree.setHeaderVisible(true);
		
		TreeColumn trclmnNewColumn = new TreeColumn(tree, SWT.NONE);
		trclmnNewColumn.setWidth(150);
		trclmnNewColumn.setText("Depart No");
		
		TreeColumn trclmnNewColumn_2 = new TreeColumn(tree, SWT.NONE);
		trclmnNewColumn_2.setWidth(100);
		trclmnNewColumn_2.setText("Emp No");
		
		TreeColumn trclmnNewColumn_3 = new TreeColumn(tree, SWT.NONE);
		trclmnNewColumn_3.setWidth(100);
		trclmnNewColumn_3.setText("First Name");
		
		TreeColumn trclmnNewColumn_4 = new TreeColumn(tree, SWT.NONE);
		trclmnNewColumn_4.setWidth(100);
		trclmnNewColumn_4.setText("Last Name");
		
		//add content and label privider
		DeptEmpTreeTableCLProvider treeTableCLProvider = new DeptEmpTreeTableCLProvider();
		treeViewer.setContentProvider(treeTableCLProvider);
		treeViewer.setLabelProvider(treeTableCLProvider);
		
		//set data source
		List<Department> departments = DataModel.getDepartments();
		treeViewer.setInput(departments);
		
		

	}

}
