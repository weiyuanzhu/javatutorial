package jfacetutorial.rs;


import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;

public class ResourceDemo {

	protected Shell shlResourceDemo;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ResourceDemo window = new ResourceDemo();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlResourceDemo.open();
		shlResourceDemo.layout();
		while (!shlResourceDemo.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlResourceDemo = new Shell();
		shlResourceDemo.setSize(450, 300);
		shlResourceDemo.setText("Resource Demo");
		
		Button btnButtonWithResource = new Button(shlResourceDemo, SWT.NONE);
		btnButtonWithResource.setImage(SWTResourceManager.getImage(ResourceDemo.class, "/jfacetutorial/image/check_sign_icon_blue.png"));
		btnButtonWithResource.setBounds(10, 10, 206, 252);
		btnButtonWithResource.setText("Button With SWT Resource Manager");
		
		Button button2 = new Button(shlResourceDemo, SWT.NONE);
		button2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		button2.setBounds(222, 10, 202, 252);
		button2.setText("Button with JFace Resource Manager");
		
		LocalResourceManager jfaceRsManager = new LocalResourceManager(
	               JFaceResources.getResources(), shlResourceDemo);
	 
	        
	       org.eclipse.swt.graphics.Color color = jfaceRsManager.createColor(new RGB(200, 100, 0));
	       org.eclipse.swt.graphics.Font font = jfaceRsManager.createFont(FontDescriptor.createFrom(
	               "Arial", 10, SWT.BOLD));
	        
	       ImageDescriptor imageDescriptor = ImageDescriptor.createFromFile(
	               ResourceDemo.class,
	               "/org/o7planning/tutorial/jface/image/check.png");
	       org.eclipse.swt.graphics.Image image = jfaceRsManager.createImage(imageDescriptor);
	 
	       button2.setFont(font);
	       button2.setBackground(color);
	       button2.setImage(image);

	}

}
