package com.mackwell.rcp3xtutorial.view;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.TableViewer;


public class TaskView extends ViewPart {
	private Table table;

	public TaskView() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new RowLayout(SWT.HORIZONTAL));
		
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new RowData(570, SWT.DEFAULT));
		
		ExpandBar expandBar = new ExpandBar(composite, SWT.NONE);
		expandBar.setBounds(10, 10, 550, 32);
		
		ExpandItem xpndtmTest = new ExpandItem(expandBar, SWT.NONE);
		xpndtmTest.setText("Test");
		

		
		Composite composite_1 = new Composite(parent, SWT.NONE);
		composite_1.setLayoutData(new RowData(570, 206));
		
		TableViewer tableViewer = new TableViewer(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setBounds(10, 10, 550, 186);
		// TODO Auto-generated method stub

	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}	
	
