package com.mackwell.rcp3xtutorial.editor;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class UserEditor extends EditorPart {
	
	public static final String ID = "com.mackwell.rcp3xtutorial.editor.user";
	private Text text;
	private Text text_1;

	public UserEditor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		if (!(input instanceof UserEditorInput)) {
			throw new PartInitException("Invalid Input: Must be " + UserEditorInput.class.getName());
		}
		
		setSite(site);
		setInput(input);

	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		
		parent.setLayout(new FillLayout());
		Composite body = new Composite(parent, SWT.NONE);
		
		Label lblNewLabel = new Label(body, SWT.NONE);
		lblNewLabel.setBounds(24, 26, 66, 21);
		lblNewLabel.setText("User Name");
		
		text = new Text(body, SWT.BORDER);
		text.setBounds(96, 26, 76, 21);
		
		Label lblEmail = new Label(body, SWT.NONE);
		lblEmail.setBounds(24, 66, 55, 15);
		lblEmail.setText("Email");
		
		text_1 = new Text(body, SWT.BORDER);
		text_1.setBounds(96, 66, 76, 21);
		
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
}
